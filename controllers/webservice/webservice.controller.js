﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var request = require('request');
var apiService = require('services/user.apiservice');
var jwt    = require('jsonwebtoken');
var nev = require('email-verification')('mongoose');
const nodemailer = require('nodemailer');
var bigInt = require("big-integer");
var http = require("http");
var https = require("https");

var BitGo = require('bitgo');
var bitgo = new BitGo.BitGo({ accessToken: config.ACCESS_TOKEN, env: config.BTC_ENVIRONMENT }); // defaults to testnet. add env: 'prod' if you want to go against mainnet

// routes
router.post('/webservice/create-user', createUser);
router.get('/webservice/list-users', listUsers);
router.post('/webservice/update-user', updateUser);
router.post('/webservice/deactivate-user', deactivateUser);
module.exports = router;


function createUser(req, res){
    apiService.createNewUser(req.body)
    .then(function(result){
        if(result){
                return res.status(200).send(JSON.stringify({ status:200,message:'success',data: result }));
            }
        })
    .catch(function (err) {
        if(err.status == 0){
            return res.status(203).send({err:err.message,status:err.status});
        }else{
            return res.status(203).send(err);    
        }
    });
}


function listUsers(req, res){
    apiService.listAllUser()
    .then(function(result){
        if(result){
                return res.status(200).send(JSON.stringify({ status:200,message:'success',data: result }));
            }
        })
    .catch(function (err) {
        if(err.status == 0){
            return res.status(203).send({err:err.message,status:err.status});
        }else{
            return res.status(203).send(err);    
        }
    }); 
}

function updateUser(req, res){
    apiService.updateUserInfo(req.body)
    .then(function(result){
        if(result){
                return res.status(200).send(JSON.stringify({ status:200,message:'success',data: result }));
            }
        })
    .catch(function (err) {
        if(err.status == 0){
            return res.status(203).send({err:err.message,status:err.status});
        }else{
            return res.status(203).send(err);    
        }
    });    
}


function deactivateUser(req, res){
    apiService.deactivateUser(req.body)
    .then(function(result){
        if(result){
                return res.status(200).send(JSON.stringify({ status:200,message:'success',data: result }));
            }
        })
    .catch(function (err) {
        if(err.status == 0){
            return res.status(203).send({err:err.message,status:err.status});
        }else{
            return res.status(203).send(err);    
        }
    });       
}