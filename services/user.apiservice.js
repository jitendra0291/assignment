﻿var mysql = require('mysql');
var config = require('config.json');
var _ = require('lodash');
const util = require('util');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var moment = require('moment');
// var helper = require('services/user.helper');
var bigInt = require("big-integer");
var useragent = require('useragent');
var _ = require('underscore');
var async = require("async");

var BitGo = require('bitgo');
var bitgo = new BitGo.BitGo({ accessToken: config.ACCESS_TOKEN, env: config.BTC_ENVIRONMENT }); // defaults to testnet. add env: 'prod' if you want to go against mainnet

/* Email Templates */
var nev = require('email-verification')('mongoose');
var EmailTemplate = require('email-templates').EmailTemplate
var path = require('path')
var appDir = path.dirname(require.main.filename);
var templateDir = path.join(appDir,'templates','welcome-email')

var user = {name: 'Joe', pasta: 'spaghetti'};



var connection = mysql.createConnection({
  host: config.Customer.dbConfig.host,
  user: config.Customer.dbConfig.user,
  password: config.Customer.dbConfig.password,
  database: config.Customer.dbConfig.dbName
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});
// db.bind('users');

var service = {};


service.createNewUser = createNewUser;
service.listAllUser = listAllUser;
service.updateUserInfo = updateUserInfo;
service.deactivateUser = deactivateUser;

module.exports = service;


function createNewUser(userParam) {
    var deferred = Q.defer();    
    var validationQry = "SELECT * FROM users WHERE email='"+userParam.email+"'";
    connection.query(validationQry, function (err, result, fields) {
    if(err) throw err;
        if(result.length == 0){
            var validatePhone = "SELECT * FROM users WHERE phone='"+userParam.phone+"'";
            connection.query(validatePhone, function (err, result, fields) {
            if(err) throw err;
                if(result.length == 0){
                    var registerInfo = registerNewUser(userParam);                      
                    deferred.resolve(registerInfo);
                }else{
                    deferred.reject({status:203,message:'Mobile number already exist.'});
                }
            });
        }else{
            deferred.reject({status:203,message:'Email already exist.'});
        }
    });
    return deferred.promise;
}

function registerNewUser(userParam){    
    var deferred = Q.defer();
    var user = _.omit(userParam, 'password');    
    user.hash = bcrypt.hashSync(userParam.password, 10);
    var loginId = ''+GUID(8)+'-'+GUID(4)+'-'+GUID(4)+'-'+GUID(12)+'';    
    var max = 999999, min = 111111;
    var datetime = moment().format('YYYY-MM-DD h:mm-ss');
    var emalToken = GUID(64),rememberToken = GUID(64);
    var otp = Math.floor(Math.random() * (max - min + 1)) + min;
    var otpSecret = GUID(64);            
    var sql = "INSERT INTO users(username,first_name, email, password,salt, phone, otp, secret,email_token,remember_token,device_id ,created_at, updated_at) VALUES ('"+loginId+"','"+user.name+"','"+user.email+"','"+user.hash+"','"+userParam.password+"','"+user.phone+"','"+otp+"' ,'"+otpSecret+"', '"+emalToken+"','"+rememberToken+"','"+user.device_id+"','"+datetime+"','"+datetime+"')";
    connection.query(sql, function (err, result) {
    if (err) throw err;
        /* insert into supported tables */
        userParam['register_id'] = result.insertId;
        userParam['otp'] = otp;
        userParam['secret'] = otpSecret;
        deferred.resolve(userParam);                                            
    });    
return deferred.promise;
}



function listAllUser(){
    var deferred = Q.defer();
    var sql = "SELECT * FROM users WHERE status=1";
    connection.query(sql, function (err, result) {
    if (err) throw err;                
        deferred.resolve(result);                                            
    });    
    return deferred.promise;   
}



function updateUserInfo(userParam){
    var deferred = Q.defer();
    var datetime = moment().format('YYYY-MM-DD h:mm-ss');
    var sql = "UPDATE users SET first_name='"+userParam.name+"', email='"+userParam.email+"',phone='"+userParam.phone+"',updated_at='"+datetime+"'";
    connection.query(sql, function (err, result) {
    if (err) throw err;        
        deferred.resolve(userParam);                                            
    });    
    return deferred.promise;
}


function deactivateUser(userParam){
    var deferred = Q.defer();
    var datetime = moment().format('YYYY-MM-DD h:mm-ss');
    var sql = "UPDATE users SET status='"+userParam.status+"' WHERE id='"+userParam.user_id+"'";
    connection.query(sql, function (err, result) {
    if (err) throw err;        
        deferred.resolve(userParam);                                            
    });    
    return deferred.promise;
}

function GUID(strLen){
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < strLen; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}
/* Send email */
// nev.configure({
//     verificationURL: ''+config.base_url+'${URL}',
//     persistentUserModel: 'Hello',
//     tempUserCollection: 'myawesomewebsite_tempusers',
 
//     transportOptions: {
//         service: 'Gmail',
//         auth: {
//             user: config.email_from,
//             pass: config.password
//         }
//     },
//     verifyMailOptions: {
//         from: 'Do Not Reply <myawesomeemail_do_not_reply@gmail.com>',
//         subject: 'Please confirm account',
//         html: 'Click the following link to confirm your account:</p><p>${URL}</p>',
//         text: 'Please confirm your account by clicking the following link: ${URL}'
//     }
// }, function(error, options){

// });
// var email = user.email;
// var URL = 'email-verification/token/'+emalToken+'';
//         nev.sendVerificationEmail(email, URL, function(err, info){
//             if(err)throw err;
//             // flash message of success 
//         });
/* Send email */


